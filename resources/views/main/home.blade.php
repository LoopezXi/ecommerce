@extends('layouts.app')

@section('title', 'Best Vacation')

@section('content')
 <div class="container text-center products-container">
 	@foreach($products as $product)
 	@include("products.product",["product" => $product]);
 	@endforeach
    <div>
 	{{$products->links()}}
 	</div>
 </div>
 
@endsection