<?php

namespace App;

class PayPal
{
	private $_apiContext;
	private $shopping_cart;
	private $_ClientId = 'ASAXk9a3V2-qC4OSSI0gphiFiCgWpVOCoy3eyR_wHos-fM0L2_-04ha5rxFhcFl4GxpKli9z9IYwcDaH';
	private $_ClientSecret = 'EGJ_WjWLALAJLxIBgT0daw0bwbuaQ5hchVx7fTz3ssLcLUgiAn1Y45XYzAdQN5v47BY2CiWOfch7gFJz';

	public function __construct($shopping_cart){

		$this->_apiContext = \PaypalPayment::ApiContext($this->_ClientId, $this->_ClientSecret);

		$config = config("paypal_payment");
		$flatConfig = array_dot($config);

		$this->_apiContext->setConfig($flatConfig);

		$this->shopping_cart = $shopping_cart;
	}

	public function generate(){
		$payment = \PaypalPayment::payment()->setIntent("sale")
							->setPayer($this->payer())
							->setTransactions([$this->transaction()])
							->setRedirectUrls($this->redirectURLs());

		try{
			$payment->create($this->_apiContext);
		}catch(\Exception $ex){
			dd($ex);
			exit(1);
		}

		return $payment;
	}

	public function payer(){
		//Returns payment's info
		return \PaypalPayment::payer()
							->setPaymentMethod("paypal");
	}

	public function redirectURLs(){
		$baseURL = url('/');
		return \PaypalPayment::redirectURLs()
							->setReturnUrl("$baseURL/payments/store")
							->setCancelUrl("$baseURL/carrito");
	}

	public function transaction(){
		//Returns transaction's info
		return \PaypalPayment::transaction()
				->setAmount($this->amount())
				->setItemList($this->items())
				->setDescription("Tu Compra")
				->setInvoiceNumber(uniqid());
	}

	public function items(){
		$items = [];

		$products = $this->shopping_cart->products()->get();

		foreach ($products as $product) {
			array_push($items, $product->paypalItem());
		}

		return \PaypalPayment::itemList()->setItems($items);
	}

    public function amount(){
    	return \PaypalPayment::amount()->setCurrency("USD")
    						->setTotal($this->shopping_cart->totalUSD());
    }

	public function execute($paymentId,$payerId){
		$payment = \PaypalPayment::getById($paymentId,$this->_apiContext);

		$execution = \PaypalPayment::PaymentExecution()
		                   ->setPayerId($payerId);

		$payment->execute($execution,$this->_apiContext);
	}






}